## Outwrite!

Creates log files for Python projects.

This will log everything sent to it. You can instruct it to only send certain lines to a partial log.

Logs can be timestamped; the format is "[HH:MM:SS] " at the beginning of each line.

Use log_line() to add a line to the logs.

Logs will close automatically when the script ends and do not need to be manually closed.

New logs will append to any existing log started on the same day.

The log initializer looks like this:

     Logger(name, [part=None, [timestamp=None, [dir=None]]])

It accepts name (string), part (bool, optional), timestamp (bool, optional), and dir (string, optional), and returns the logger object.

    *name* will be the filename prefix (<name>-MM-DD-YY.log).
        There is no default; this must be submitted as an argument.
    *part* determines whether there will be a "partial" log.
        This is useful if you want to log both everything
        that happens and pull out certain specific events.
        The default is False; it will not create a partial log
        unless you tell it to.
    *timestamp* determines whether lines in the log should be
        timestamped. The stamp will be based on server time.
        The default is True; it will add a timestamp unless
        you tell it not to.
    *dir* should be a directory path in which to put the logs.
        It can be absolute or relative.
        If it doesn't end with "/", one will be added.
        The default is './logs/'.
        The logger will create the directory if it doesn't exist.

To log a line, use the log_line() function.

    log_line(self, line, partial=False)

It accepts a line of text and a boolean and returns nothing. The partial log will only be written to if *partial* is set true here AND if the partial log was initialized at startup.

The logger class has several private functions that are for internal use only.
