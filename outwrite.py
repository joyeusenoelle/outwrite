from datetime import datetime
import os

class Logger:
    """ Creates log files for Python projects.
        Will log everything sent to it. You can instruct it to only
        send certain lines to a partial log.
        All logs are timestamped.
        Use log_line() to add a line to the logs.
        Logs will close automatically when the script ends and do not
        need to be manually closed.
        New logs will append to any existing log started on the same day.
    """
    def __init__(self, name, part=None, timestamp=None, dir=None):
        """ Accepts name (string), part (bool, optional), timestamp (bool,
            optional), and dir (string, optional)
            Returns the object.
            name will be the filename prefix (<name>-MM-DD-YY.log).
            part determines whether there will be a "partial" log.
                This is useful if you want to log both everything
                that happens and pull out certain specific events.
            timestamp determines whether lines in the log should be
                timestamped. The stamp will be based on server time.
            dir should be a directory path in which to put the logs.
                It can be absolute or relative.
                If it doesn't end with "/", one will be added.
        """
        self._name = name
        self._part = bool(part)
        if timestamp == None:
            self._ts = True
        else:
            self._ts = bool(timestamp)
        if dir == None:
            self._dir = "logs/"
        else:
            self._dir = dir
            if dir[-1] != "/":
                self._dir += "/"
        if not os.path.exists(self._dir):
            os.makedirs(self._dir)
        tn = datetime.now()
        self._date = "{}-{}-{}".format(tn.year,
                                       self.zeroize(tn.month),
                                       self.zeroize(tn.day))
        self._start_log()

    def _start_log(self):
        """ Initializes the log files.
            Accepts no parameters.
            Returns nothing.
            If the logger is using a partial log, this will create that log too.l
        """
        self._full_log_name = "{}{}-{}.log".format(self._dir,
                                                        self._name,
                                                        self._date)
        with open(self._full_log_name, "a+") as file:
            file.write("{}Log started.\n".format(self.timestamp()))
        if self._part:
            self._part_log_name = "{}{}-{}.partial.log".format(self._dir,
                                                               self._name,
                                                               self._date)
            with open(self._part_log_name, "a+") as file:
                file.write("{}Log started.\n".format(self.timestamp()))

    def log_line(self, line, partial=False):
        """ Adds a line to the logs.
            Accepts a line of text and a boolean.
            Returns nothing.
            The partial log will only be written to if partial is set true
                here AND if the partial log was initialized at startup.
        """
        with open(self._full_log_name, "a+") as file:
            file.write("{}{}\n".format(self.timestamp(), line))
        if self._part and partial:
            with open(self._part_log_name, "a+") as file:
                file.write("{}{}\n".format(self.timestamp(), line))

    def _zeroize(self, n):
        """ Returns a string containing a number prefixed with 0 if necessary.
            Accepts an int.
            Returns a string.
            If the int is less than 10, add a preceding 0.
        """
        return "0{}".format(n) if n < 10 else "{}".format(n)

    def _timestamp(self):
        """ Returns a string containing a timestamp OR an empty string.
            Accepts no parameters.
            Returns a string.
            If the logger was initialized with timestamp=False, this
                will return an empty string. Otherwise it will return
                "[HH:MM:SS] " (including the space). Timestamps are
                based on server time.
        """
        if self._ts == False:
            return ""
        tn = datetime.now()
        return "[{}:{}:{}]".format(self.zeroize(tn.hour),
                                   self.zeroize(tn.minute),
                                   self.zeroize(tn.second))
